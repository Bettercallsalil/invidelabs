import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Animated,
  Image,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
import Dimensions from 'Dimensions';
const devWidth=Dimensions.get('window').width;
const devHeight=Dimensions.get('window').height;
import * as Progress from 'react-native-progress';

class App extends Component {
   constructor(props) {
      super(props); 
      
    this.state = {
       loading:true,
       timerStarted:false,
       timeRemaining:60
      }  
    }
    

    componentDidMount(){
      this.setState({
        loading:false  
      })
    }
    startTimer(){
      let progress=0
      this.setState({
        timerStarted:true,
        progress:0
      })          
      this._interval=setInterval(()=>{
        progress += 1/60
        this.setState({ progress });
      this.setState({
        timeRemaining:this.state.timeRemaining-1
      })
    },1000)
       setTimeout(()=>{
          clearInterval(this._interval);
          this.setState({
            timerStarted:false,
            timeRemaining:60
          })    
       },61000)  
    }
  render() {
    if(this.state.loading){
        return(
            <View style={styles.loader}>
              <ActivityIndicator size='large' animating={true} />
            </View>
          );
      }
    return (
      <Animated.View style={styles.container}>  
          <Image source={{uri: "https://res.cloudinary.com/qdesqtest/image/upload/v1477034577/shutterstock_328978469_djcdy4.jpg"}} style={styles.headImage} />
          <View style={styles.contentContainer}>
            <Text style={styles.title}>Your Goal</Text> 
            <Text style={styles.description}>Improving Personality</Text>  
            <Text style={styles.title}>Time Remaining</Text> 
            <Text style={styles.description}>{this.state.timeRemaining}</Text>            
            <View style={{alignItems:'center'}}>
              <Progress.Circle showsText={true} textStyle={{color:'black'}} style={styles.timerStyle} thickness={6} progress={0.3} size={devWidth/3} color={'#ffae19'} progress={!this.state.timerStarted?0:this.state.progress} />            
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={!this.state.timerStarted?this.startTimer.bind(this):null}>
              <View style={styles.button}>
                  <Text style={styles.buttonText}>{this.state.timerStarted?'Running..':'Lets Get Shit Done'}</Text>
              </View>
              </TouchableOpacity>
            </View>
          </View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#FFF',
  },
  contentContainer:{
    flex:1,
    padding:50,
    height:devHeight*3/4  
  },
  title:{
    marginTop:20,
    fontSize:13,
    fontWeight:'700',
    color:'#a8a8a8'
  },
  timerStyle:{
    marginTop:40
  },
  buttonContainer:{
    marginTop:70,   
  },
  button:{
    padding:15,
    borderRadius:5,
    backgroundColor:'rgb(0,168,251)',
    borderColor:'rgb(0,168,251)'
  },
  buttonText:{
    color:'#fff',
    alignSelf:'center',
    fontWeight:'400'
  },
  description:{
    fontSize:20,
    fontWeight:'500',
    marginTop:10  
    
  },
  headImage:{
    width:devWidth,
    height:devHeight/4  
  },
  loader:{
    flex: 1, 
    margin:10,
    justifyContent: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

module.exports = App;
